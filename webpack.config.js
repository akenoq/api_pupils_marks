"use strict";

const nodeExternals = require("webpack-node-externals"); // for require

module.exports = {
    target: "node",
    externals: [nodeExternals()],
    entry: "./scripts/server.js",
    output: {
        filename: "./bundle/app.js"
    }
};
