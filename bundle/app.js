/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = debugLog;
function debugLog(s) {
    console.log(s);
}

/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__globalBus__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__QueryMaker__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__debugLog__ = __webpack_require__(0);






let pg = __webpack_require__(4);
let fs = __webpack_require__(5);

let express = __webpack_require__(6);
let app = express();

let qm = new __WEBPACK_IMPORTED_MODULE_1__QueryMaker__["a" /* default */](app, pg, fs);
Object(__WEBPACK_IMPORTED_MODULE_0__globalBus__["a" /* default */])().qm = qm;

function reset_db(callback) {
    let cont =
        'DROP TABLE IF EXISTS mark;\n' +
        'DROP TABLE IF EXISTS pupil;\n' +
        'DROP TABLE IF EXISTS subject;\n' +
        '\n' +
        'CREATE TABLE IF NOT EXISTS pupil (\n' +
        '    p_id serial  PRIMARY KEY not null,\n' +
        '    p_nickname text  unique,\n' +
        '    p_age integer\n' +
        ');\n' +
        '\n' +
        'CREATE TABLE IF NOT EXISTS subject (\n' +
        '    s_id serial  PRIMARY KEY not null,\n' +
        '    s_name text unique,\n' +
        '    s_description text\n' +
        ');\n' +
        '\n' +
        'CREATE TABLE IF NOT EXISTS mark (\n' +
        '    m_id serial  PRIMARY KEY not null,\n' +
        '    m_mark integer,\n' +
        '    p_id integer REFERENCES pupil(p_id) ON DELETE CASCADE not null,\n' +
        '    s_id integer REFERENCES subject(s_id) ON DELETE CASCADE not null\n' +
        ');\n';
    qm.request(cont, [],
        (err) => {
            console.log(`err: __reset__ ${err}`);
            callback(err,  null);
        },
        (arr) => {
            console.log(arr);
            callback(null, arr);
        }
    )
}

reset_db(() => {});

/**
 * postHandler
 * @param req
 * @param callback (object)
 */
function postHandler(req, callback) {
    let dataStr = '';
    req.on('data', (data) => {
        dataStr += data;
    }).on('end', () => {
        callback(dataStr === '' ? null : JSON.parse(dataStr))
    })
}

function getSortTypeSql(sortParam) {
    return parseInt(sortParam) === 1 ? 'ASC' : 'DESC';
}

app.post('/database/clear',(req, resp) => {
    postHandler(req, (obj) => {
        reset_db((err, ans) => {
            if (!err) {
                resp.status(200).json({
                    result: "OK"
                })
            }
        });
    });
});

app.post('/subjects/add', (req, resp) => {
    postHandler(req, (obj) => {
        qm.request(
            'INSERT INTO subject (s_name, s_description) VALUES ($1, $2)',
            [obj.subject, obj.description],
            (err) => {
                qm.request(
                    'SELECT s_name, s_description FROM subject WHERE s_name = $1',
                    [obj.subject],
                    (err) => {},
                    (arr) => {
                        resp.json({
                            subject: arr[0].s_name,
                            description: arr[0].s_description
                        })
                    }
                )
            },
            (arr) => resp.json({
                result: "OK"
            })
        )
    })
});

app.get('/subjects/get', (req, resp) => {
    qm.request(
        'SELECT s_name, s_description FROM subject WHERE s_name = $1',
        [req.query.subject],
        (err) => { },
        (arr) => {
            if (arr.length !== 0) {
                resp.json({
                    subject: arr[0].s_name,
                    description: arr[0].s_description
                })
            } else {
                resp.json({
                    result: "NOT_FOUND"
                })
            }
        }
    )
});

app.get('/subjects/get/all', (req, resp) => {
    let sort = getSortTypeSql(req.query.sort);
    qm.request(
        'SELECT * FROM subject ORDER BY s_id ' + sort,
        [],
        (err) => {},
        (arr) => {
            let arrRes = arr.map((elem) => {
                let newObj = {
                    subject: elem.s_name,
                    description: elem.s_description
                };
                return newObj;
            });
            resp.json(
                arrRes
            )
        }
    )
});

app.get('/pupils/get/count', (req, resp) => {
    qm.request(
        'SELECT COUNT(p_nickname) FROM pupil',
        [],
        (err) => { },
        (arr) => {
            // parseInt!
            resp.json({
                count: parseInt(arr[0].count)
            })
        }
    )
});

app.post('/pupils/add', (req, resp) => {
    postHandler(req, (obj) => {
        qm.request(
            'INSERT INTO pupil (p_nickname, p_age) VALUES ($1, $2)',
            [obj.nickname, obj.age],
            (err) => {
                qm.request(
                    'SELECT p_nickname, p_age FROM pupil WHERE p_nickname = $1',
                    [obj.nickname],
                    (err) => { },
                    (arr) => {
                        resp.json({
                            nickname: arr[0].p_nickname,
                            age: arr[0].p_age
                        })
                    }
                )
            },
            (arr) => resp.json({
                result: 'OK'
            })
        )
    })
});

app.get('/pupils/get/all', (req, resp) => {
    let sort = getSortTypeSql(req.query.sort);
    qm.request(
        'SELECT p_nickname, p_age FROM pupil ORDER BY p_id ' + sort,
        [],
        (err) => { },
        (arr) => {
            let resArr = arr.map((elem) => {
                return {
                    nickname: elem.p_nickname,
                    age: elem.p_age
                }
            });
            resp.json(resArr);
        }
    )
});

// {"nickname":"Maxim","subject":"mathematics","mark":5}
app.post('/marks/add', (req, resp) => {
    let p_id = -1;
    let s_id = -1;
    postHandler(req, (obj) => {
        // проверить есть ли Максим, есть ли Предмет
        // стоит ли делать селект запросы параллельно?
        // err - только в INSERT и UPDATE
        qm.request(
            'SELECT p_id FROM pupil WHERE p_nickname = $1',
            [obj.nickname],
            (err) => { },
            (arr) => {
                if (arr.length !== 0) {
                    p_id = parseInt(arr[0].p_id);
                    qm.request(
                        'SELECT s_id FROM subject WHERE s_name = $1',
                        [obj.subject],
                        (err) => { },
                        (arr) => {
                            if (arr.length !== 0) {
                                s_id = parseInt(arr[0].s_id);
                                qm.request(
                                    'INSERT INTO mark (m_mark, p_id, s_id) VALUES ($1, $2, $3)',
                                    [obj.mark, p_id, s_id],
                                    (err) => {
                                    },
                                    (arr) => {
                                        resp.json({
                                            result: 'OK'
                                        })
                                    }
                                )
                            } else {
                                resp.json({
                                    result: 'BAD_NICKNAME_OR_SUBJECT'
                                })
                            }
                        }
                    )
                } else {
                    resp.json({
                        result: 'BAD_NICKNAME_OR_SUBJECT'
                    })
                }
            }
        )
    })
});

// /marks/get?nickname=Maxim&subject=mathematics&sort=1
app.get('/marks/get', (req, resp) => {
    let nickname = req.query.nickname;
    let subject = req.query.subject;
    let sort = getSortTypeSql(req.query.sort);
    Object(__WEBPACK_IMPORTED_MODULE_2__debugLog__["a" /* default */])(sort);
    qm.request(
        'SELECT m_mark FROM mark ' +
        'INNER JOIN pupil ON pupil.p_id = mark.p_id ' +
        'INNER JOIN subject ON subject.s_id = mark.s_id ' +
        'WHERE pupil.p_nickname = $1 AND subject.s_name = $2' +
        'ORDER BY m_id ' + sort,
        [nickname, subject],
        (err) => { Object(__WEBPACK_IMPORTED_MODULE_2__debugLog__["a" /* default */])(err) },
        (arr) => {
            Object(__WEBPACK_IMPORTED_MODULE_2__debugLog__["a" /* default */])(arr)
            if (arr.length !== 0) {
                let arrRes = arr.map((elem) => {
                    let newElem = {
                        mark: elem.m_mark
                    };
                    return newElem;
                });
                resp.json(arrRes);
            } else {
                resp.end('[]');
            }
        }
    )
});

let port = 5007;
app.listen(port);
Object(__WEBPACK_IMPORTED_MODULE_2__debugLog__["a" /* default */])(`server works on port = ${port}`);


/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = globalBus;
const bus = {};

function globalBus () {
    return bus;
}

/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__debugLog__ = __webpack_require__(0);


class QueryMaker {
    constructor(app, pg, fs) {
        this.app = app;
        this.pg = pg;
        this.fs = fs;

        // sudo -u postgres psql
        // CREATE DATABASE my_1;
        const pool = new pg.Pool({
            user: 'postgres',
            host: 'localhost',
            database: 'pupils_marks',
            password: '12345',
            port: 5432
        });

        pool.on('error', (err, client) => {
            __WEBPACK_IMPORTED_MODULE_0__debugLog__["a" /* default */].log("_____POOL_____ERROR_____");
        });

        this.pool = pool;
    }

    /**
     * Request with values array to pg
     * @param queryString - template sql query
     * @param values - array of parameters for template
     * @param callbackError
     * @param callbackResp
     */
    request(queryString, values, callbackError, callbackResp) {
        const pool = this.pool;
        let respObj = {
            arr: []
        };
        pool.query(queryString, values, (err, res) => {
            if(err !== null) {
                Object(__WEBPACK_IMPORTED_MODULE_0__debugLog__["a" /* default */])("callbackError");
                callbackError(err);
            } else {
                Object(__WEBPACK_IMPORTED_MODULE_0__debugLog__["a" /* default */])("callbackNormal");
                respObj.arr = res.rows;
                callbackResp(respObj.arr);
            }
        });
    }

}
/* harmony export (immutable) */ __webpack_exports__["a"] = QueryMaker;


/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = require("pg");

/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = require("fs");

/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = require("express");

/***/ })
/******/ ]);